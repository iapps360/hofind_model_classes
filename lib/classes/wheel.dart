import 'package:json_annotation/json_annotation.dart';
import 'classes.dart';
import 'base_serializable.dart';

part 'wheel.g.dart';

@JsonSerializable()
class Wheel extends BaseSerializable {
  /// 獎項
  String name;

  /// Type 如果是
  /// none: 未中獎
  /// item: 實體獎品
  /// coin: 好康幣
  /// point:紅利點數
  String type;

  ImageInfo img;///只有實體獎品有img
  String val; /// 好康幣/紅利點數數量
  String content; ///在獎項清單中用的詳細資訊

  Wheel({this.name, this.type, this.img, this.val});

  factory Wheel.fromJson(json) => _$WheelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$WheelToJson(this);
}