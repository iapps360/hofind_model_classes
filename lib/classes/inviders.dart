import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'classes.dart';

part 'inviders.g.dart';

@JsonSerializable()
class  Inviders extends BaseSerializable {

int first;
int second;
int total;
List<User> firstList;
List<User> secondList;

  Inviders({this.first, this.second, this.total, this.firstList, this.secondList});

  factory Inviders.fromJson(Map<String, dynamic> json) =>
      _$InvidersFromJson(json);

  @override
  toJson() => _$InvidersToJson(this);

}

