import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'pagination.g.dart';

@JsonSerializable()
class Pagination extends BaseSerializable {

  int index;
  int limit;
  int total;
  int size;

  Pagination({this.index, this.limit, this.total, this.size});

  factory Pagination.fromJson(Map<String, dynamic> json) =>
      _$PaginationFromJson(json);

  @override
  toJson() => _$PaginationToJson(this);

}
