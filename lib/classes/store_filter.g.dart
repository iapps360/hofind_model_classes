// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_filter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StoreFilter _$StoreFilterFromJson(Map<String, dynamic> json) {
  return StoreFilter(
    active: json['active'] as bool,
    location: json['location'] == null
        ? null
        : FilterLocation.fromJson(json['location'] as Map<String, dynamic>),
    keyword: json['keyword'] as String,
    catalog: json['catalog'] as String,
    tag: json['tag'] as String,
    indicate: json['indicate'] == null
        ? null
        : Indicate.fromJson(json['indicate'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$StoreFilterToJson(StoreFilter instance) =>
    <String, dynamic>{
      'active': instance.active,
      'location': instance.location,
      'keyword': instance.keyword,
      'catalog': instance.catalog,
      'tag': instance.tag,
      'indicate': instance.indicate,
    };

FilterLocation _$FilterLocationFromJson(Map<String, dynamic> json) {
  return FilterLocation(
    lng: (json['lng'] as num)?.toDouble(),
    latt: (json['latt'] as num)?.toDouble(),
    dist: (json['dist'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$FilterLocationToJson(FilterLocation instance) =>
    <String, dynamic>{
      'lng': instance.lng,
      'latt': instance.latt,
      'dist': instance.dist,
    };
