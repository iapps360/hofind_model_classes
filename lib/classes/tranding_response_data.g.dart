// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tranding_response_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrandingResData _$TrandingResDataFromJson(Map<String, dynamic> json) {
  return TrandingResData(
    amount: json['amount'] == null
        ? null
        : _Amount.fromJson(json['amount'] as Map<String, dynamic>),
    discount: json['discount'] == null
        ? null
        : _Discount.fromJson(json['discount'] as Map<String, dynamic>),
    store: json['store'] == null
        ? null
        : StoreDetail.fromJson(json['store'] as Map<String, dynamic>),
    uid: json['uid'] as String,
    storeUid: json['storeUid'] as String,
    state: json['state'] as String,
    no: json['no'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    takeoutTime: json['takeoutTime'] as String,
    orderList: (json['orderList'] as List)
        ?.map((e) => e == null
            ? null
            : CarryoutOrder.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    takeout: json['takeout'] as bool,
  )..takeoutTimeModify = json['takeoutTimeModify'] as bool;
}

Map<String, dynamic> _$TrandingResDataToJson(TrandingResData instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'discount': instance.discount,
      'store': instance.store,
      'uid': instance.uid,
      'storeUid': instance.storeUid,
      'state': instance.state,
      'no': instance.no,
      'createdAt': instance.createdAt?.toIso8601String(),
      'takeoutTimeModify': instance.takeoutTimeModify,
      'takeoutTime': instance.takeoutTime,
      'orderList': instance.orderList,
      'takeout': instance.takeout,
    };

_Amount _$_AmountFromJson(Map<String, dynamic> json) {
  return _Amount(
    pay: (json['pay'] as num)?.toDouble(),
    total: (json['total'] as num)?.toDouble(),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
  );
}

Map<String, dynamic> _$_AmountToJson(_Amount instance) => <String, dynamic>{
      'pay': instance.pay,
      'total': instance.total,
      'createdAt': instance.createdAt?.toIso8601String(),
    };

_Discount _$_DiscountFromJson(Map<String, dynamic> json) {
  return _Discount(
    coin: (json['coin'] as num)?.toDouble(),
    direct: (json['direct'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$_DiscountToJson(_Discount instance) => <String, dynamic>{
      'coin': instance.coin,
      'direct': instance.direct,
    };
