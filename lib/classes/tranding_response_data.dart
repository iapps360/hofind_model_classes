import 'classes.dart';
import 'package:json_annotation/json_annotation.dart';
import 'store_detail.dart';
import 'base_serializable.dart';

part 'tranding_response_data.g.dart';

@JsonSerializable()
class TrandingResData extends BaseSerializable {
  _Amount amount;
  _Discount discount;
  StoreDetail store;
  String uid;
  String storeUid;
  String state;
  String no;
  DateTime createdAt;

  /// takeoutTimeIsModify is going to indicate the takeout time has been change by store
  bool takeoutTimeModify;

  /// takeoutTime only available when takeout enable
  String takeoutTime;

  /// orderList only available when takeout enable
  List<CarryoutOrder> orderList;

  /// to indicate the store takeout function has enabled
  bool takeout;

  TrandingResData(
      {this.amount,
      this.discount,
      this.store,
      this.uid,
      this.storeUid,
      this.state,
      this.no,
      this.createdAt,
      this.takeoutTime,
      this.orderList,
      this.takeout});

  @override
  Map<String, dynamic> toJson() => _$TrandingResDataToJson(this);

  factory TrandingResData.fromJson(Map<String, dynamic> json) =>
      _$TrandingResDataFromJson(json);
}

@JsonSerializable()
class _Amount extends BaseSerializable {
  double pay;
  double total;
  DateTime createdAt;

  _Amount({this.pay, this.total, this.createdAt});

  @override
  Map<String, dynamic> toJson() => _$_AmountToJson(this);

  factory _Amount.fromJson(Map<String, dynamic> json) =>
      _$_AmountFromJson(json);
}

@JsonSerializable()
class _Discount extends BaseSerializable {
  double coin;
  double direct;

  _Discount({this.coin, this.direct});

  @override
  Map<String, dynamic> toJson() => _$_DiscountToJson(this);

  factory _Discount.fromJson(Map<String, dynamic> json) =>
      _$_DiscountFromJson(json);
}
