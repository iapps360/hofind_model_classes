// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inviders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Inviders _$InvidersFromJson(Map<String, dynamic> json) {
  return Inviders(
    first: json['first'] as int,
    second: json['second'] as int,
    total: json['total'] as int,
    firstList: (json['firstList'] as List)
        ?.map((e) => e == null ? null : User.fromJson(e))
        ?.toList(),
    secondList: (json['secondList'] as List)
        ?.map((e) => e == null ? null : User.fromJson(e))
        ?.toList(),
  );
}

Map<String, dynamic> _$InvidersToJson(Inviders instance) => <String, dynamic>{
      'first': instance.first,
      'second': instance.second,
      'total': instance.total,
      'firstList': instance.firstList,
      'secondList': instance.secondList,
    };
