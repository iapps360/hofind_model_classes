import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'credit_card.g.dart';

@JsonSerializable()
class CreditCard extends BaseSerializable {
  /// credit card number
  String no;

  /// a expired date for this credit card
  DateTime expired;

  CreditCard({
    this.no,
    this.expired,
  });

  factory CreditCard.fromJson(Map<String, dynamic> json) =>
      _$CreditCardFromJson(json);

  Map<String, dynamic> toJson() => _$CreditCardToJson(this);

}
