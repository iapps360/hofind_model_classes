
import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'img.g.dart';

@JsonSerializable()
class Img extends BaseSerializable {

  String uid;
  String small;
  String medium;
  String large;

  Img({this.uid, this.small, this.medium, this.large});

  factory Img.fromJson(Map<String, dynamic> json) =>
      _$ImgFromJson(json);

  @override
  toJson() => _$ImgToJson(this);

}

