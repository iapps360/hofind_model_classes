import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'img.dart';
part 'prize_gift_record.g.dart';

@JsonSerializable()
class PrizeGiftRecord extends BaseSerializable {
  String state; //finished(已發送,好康幣等虛擬幣),waiting(登入領取),processing(處理中),send(已寄出,實體禮物),cancel(取消)
  String name;
  String type;
  String userUid;
  String refUid;
  String uid;
  String createdAt;
  String updatedAt;
  num val;
  Img img;
  
  PrizeGiftRecord({this.state, this.name, this.type, this.userUid, this.refUid, this.uid, this.createdAt, this.updatedAt, this.val, this.img});

  factory PrizeGiftRecord.fromJson(Map<String, dynamic> json) =>
      _$PrizeGiftRecordFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PrizeGiftRecordToJson(this);
}
