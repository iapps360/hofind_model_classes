import 'carryout/menu.dart';
import 'package:json_annotation/json_annotation.dart';
import 'base_serializable.dart';
import 'store.dart';
import 'socialURL.dart';

part 'store_detail.g.dart';

@JsonSerializable()
class StoreDetail extends BaseSerializable {
  bool active;
  String address;
  String agentUid;
  Bank bank;
  String catalog;
  Contact contact;
  DateTime createdAt;
  Credit credit;
  String currency;
  ImageInfo logo;
  List<ImageInfo> imgList;
  Invoice invoice;
  String level;
  Location location;
  List menu;
  String name;
  List<OpenTime> openTime;
  Owner owner;
  double refund;
  List<String> tag;
  String telephone;
  String uid;
  double discountDirect;
  DateTime updatedAt;
  Comment comment;
  Indicate indicate;
  String remark;
  String introduction;
  String political;
  String post;
  double firstRefund;
  double secondRefund;
  double areaRefund;
  double agentRefund;
  String areaUid;
  Map<String, List<Menu>> menuList;
  String takeoutTime;

  @JsonKey(name: 'link')
  SocialURL socialURL;

  StoreDetail({
    this.active,
    this.address,
    this.agentUid,
    this.bank,
    this.catalog,
    this.contact,
    this.createdAt,
    this.credit,
    this.currency,
    this.logo,
    this.imgList,
    this.invoice,
    this.level,
    this.location,
    this.menu,
    this.name,
    this.openTime,
    this.owner,
    this.refund,
    this.tag,
    this.telephone,
    this.uid,
    this.discountDirect,
    this.updatedAt,
    this.comment,
    this.indicate,
    this.remark,
    this.introduction,
    this.socialURL,
    this.political,
    this.post,
    this.firstRefund,
    this.secondRefund,
    this.areaRefund,
    this.agentRefund,
    this.areaUid,
    this.takeoutTime,
  });

  factory StoreDetail.fromJson(Map<String, dynamic> json) =>
      _$StoreDetailFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$StoreDetailToJson(this);
}

@JsonSerializable()
class Bank extends BaseSerializable {
  String account;
  String code;
  String name;

  Bank({this.account, this.code, this.name});

  factory Bank.fromJson(Map<String, dynamic> json) => _$BankFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$BankToJson(this);
}

@JsonSerializable()
class Contact extends BaseSerializable {
  String name;
  String telephone;
  String mail;

  Contact({this.mail, this.name, this.telephone});

  factory Contact.fromJson(Map<String, dynamic> json) =>
      _$ContactFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ContactToJson(this);
}

@JsonSerializable()
class Credit extends BaseSerializable {
  String no;
  DateTime expired;
  String type;

  Credit({this.type, this.no, this.expired});

  factory Credit.fromJson(Map<String, dynamic> json) => _$CreditFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CreditToJson(this);
}

@JsonSerializable()
class Invoice extends BaseSerializable {
  String type;

  Invoice({this.type});

  factory Invoice.fromJson(Map<String, dynamic> json) =>
      _$InvoiceFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$InvoiceToJson(this);
}

@JsonSerializable()
class Owner extends BaseSerializable {
  String iso;
  String mail;
  String address;
  String id;
  String name;
  String telephone;

  Owner(
      {this.iso, this.mail, this.address, this.id, this.name, this.telephone});

  factory Owner.fromJson(Map<String, dynamic> json) => _$OwnerFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$OwnerToJson(this);
}

/// 其他顯示項目　other information to show on screen
@JsonSerializable()
class Indicate extends BaseSerializable {
  /// 不包含酒水類
  bool wine;
  bool linkFb;
  bool linkOffical;
  bool linkIg;
  bool takeout;
  bool top;
  bool show;

  Indicate(
      {this.show,
      this.wine,
      this.linkFb,
      this.linkOffical,
      this.linkIg,
      this.takeout,
      this.top});

  factory Indicate.fromJson(Map<String, dynamic> json) =>
      _$IndicateFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$IndicateToJson(this);
}
