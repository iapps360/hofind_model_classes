import 'package:json_annotation/json_annotation.dart';

part 'socialURL.g.dart';

@JsonSerializable()
class SocialURL {
  String fb;
  String ig;

  @JsonKey(name: 'offical')
  String officialWebsite;

  SocialURL({this.fb, this.officialWebsite, this.ig});

  factory SocialURL.fromJson(Map<String, dynamic> json) =>
      _$SocialURLFromJson(json);

  Map<String, dynamic> toJson() => _$SocialURLToJson(this);
}
