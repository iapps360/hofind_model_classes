import 'dart:core';
import 'classes.dart';
import 'package:json_annotation/json_annotation.dart';
import 'base_serializable.dart';

part 'store_filter.g.dart';

enum FilterCatalog {
  food,
  clothing,
  housing,
  transportation,
  education,
  entertainment
}

const List<String> STORE_CATEGORY = [  "food",
  "clothing",
  "housing",
  "transportation",
  "education",
  "entertainment"
];

@JsonSerializable()
class StoreFilter extends BaseSerializable{
  bool active;
  FilterLocation location;
  String keyword;
  String catalog;
  String tag;
  Indicate indicate;

  StoreFilter(
      {this.active, this.location, this.keyword, this.catalog, this.tag, this.indicate});

  factory StoreFilter.fromJson(Map<String, dynamic> json) =>
      _$StoreFilterFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$StoreFilterToJson(this);
}

@JsonSerializable()
class FilterLocation extends BaseSerializable {
  double lng;
  double latt;
  double dist;

  FilterLocation({this.lng, this.latt, this.dist});

  factory FilterLocation.fromJson(Map<String, dynamic> json) =>
      _$FilterLocationFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$FilterLocationToJson(this);
}
