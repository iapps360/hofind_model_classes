import 'package:json_annotation/json_annotation.dart';
import 'classes.dart';
import 'base_serializable.dart';

part 'gift_rec.g.dart';

@JsonSerializable()
class GiftRec extends BaseSerializable {
  /**
   * status
   *   finished  　// 已發送（紅利點／好康幣　已發送）
   *   waiting　　　// 等待填寫資料
   *   processing　// 處理中
   *   send　　　　　// 已寄送
   *   cancel　　　　// 已取消
   * **/
  String state;

  /// 獎項
  String name;

  /// Type 如果是
  /// none: 未中獎
  /// item: 實體獎品
  /// coin: 好康幣
  /// point:紅利點數
  /// gift: 兌換獎品
  String type;

  ImageInfo img;

  ///只有實體獎品有img
  String val;

  /// 好康幣/紅利點數數量
  String content;

  ///在獎項清單中用的詳細資訊
  GiftRec({this.name, this.type, this.img, this.val});

  factory GiftRec.fromJson(json) => _$GiftRecFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GiftRecToJson(this);
}
