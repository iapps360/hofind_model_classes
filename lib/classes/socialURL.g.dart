// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'socialURL.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SocialURL _$SocialURLFromJson(Map<String, dynamic> json) {
  return SocialURL(
    fb: json['fb'] as String,
    officialWebsite: json['offical'] as String,
    ig: json['ig'] as String,
  );
}

Map<String, dynamic> _$SocialURLToJson(SocialURL instance) => <String, dynamic>{
      'fb': instance.fb,
      'ig': instance.ig,
      'offical': instance.officialWebsite,
    };
