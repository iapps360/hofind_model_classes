import 'classes.dart';
import 'package:json_annotation/json_annotation.dart';
import 'base_serializable.dart';
import 'phone_number.dart';
import 'img.dart';

part 'user.g.dart';

//unknow male female
enum Gender { Male, Female, Unknow }

@JsonSerializable()
class User extends BaseSerializable {
  String uid;
  String id; //身分證字號
  PhoneNumber phone;
  String email;
  String first_name;
  String last_name;
  String name;
  String gender;
  DateTime birthday;
  String post;
  String contry;
  String city;
  String district;
  String address;
  DateTime bindDate;
  DateTime createdAt;
  DateTime updatedAt;
  Invite invite;
  Img img;
  CreditCard credit;

  User({
      this.uid,
      this.id,
      this.phone,
      this.email,
      this.name,
      this.first_name,
      this.last_name,
      this.gender,
      this.birthday,
      this.post,
      this.contry,
      this.city,
      this.district,
      this.address,
      this.createdAt,
      this.updatedAt,
      this.img});

  factory User.fromJson(json) {
    User user = _$UserFromJson(json);
    Map first = json['introduce']['first'];
    if (first.containsKey('uid')) {
      user.invite =
          Invite(uid: first['uid'], type: first['type'], rate: first['rate']);
    }
    return user;
  }

  @override
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class Invite {
  String uid;
  String type;
  num rate;

  Invite({this.uid, this.type, this.rate});

  factory Invite.fromJson(json) => _$InviteFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$InviteToJson(this);
}
