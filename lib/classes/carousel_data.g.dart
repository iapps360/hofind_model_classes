// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'carousel_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarouselData _$CarouselDataFromJson(Map<String, dynamic> json) {
  return CarouselData(
    type: json['type'] as String,
    img: json['img'] == null
        ? null
        : ImageInfo.fromJson(json['img'] as Map<String, dynamic>),
    storeUid: json['storeUid'] as String,
    link: json['link'] as String,
    eventUid: json['eventUid'] as String,
  );
}

Map<String, dynamic> _$CarouselDataToJson(CarouselData instance) =>
    <String, dynamic>{
      'type': instance.type,
      'img': instance.img,
      'storeUid': instance.storeUid,
      'link': instance.link,
      'eventUid': instance.eventUid,
    };
