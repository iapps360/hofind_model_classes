// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Settings _$SettingsFromJson(Map<String, dynamic> json) {
  return Settings(
    active: json['active'] as bool,
    time: json['time'] == null ? null : DateTime.parse(json['time'] as String),
    limit: json['limit'] as int,
    catalog: (json['catalog'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$SettingsToJson(Settings instance) => <String, dynamic>{
      'active': instance.active,
      'time': instance.time?.toIso8601String(),
      'limit': instance.limit,
      'catalog': instance.catalog,
    };
