// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'carryout_order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarryoutOrder _$CarryoutOrderFromJson(Map<String, dynamic> json) {
  return CarryoutOrder(
    name: json['name'] as String,
    require: (json['require'] as List)
        ?.map((e) => e == null
            ? null
            : MenuRequireOrderItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    optional: (json['optional'] as List)
        ?.map((e) =>
            e == null ? null : MenuOptional.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    num: json['num'] as int,
    price: (json['price'] as num)?.toDouble(),
    remark: json['remark'] as String,
    requireTemp: (json['requireTemp'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k,
          e == null ? null : MenuOptional.fromJson(e as Map<String, dynamic>)),
    ),
    menu: json['menu'] == null
        ? null
        : Menu.fromJson(json['menu'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CarryoutOrderToJson(CarryoutOrder instance) =>
    <String, dynamic>{
      'name': instance.name,
      'require': instance.require,
      'optional': instance.optional,
      'num': instance.num,
      'price': instance.price,
      'remark': instance.remark,
      'menu': instance.menu,
      'requireTemp': instance.requireTemp,
    };
