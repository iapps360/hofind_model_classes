import 'dart:core';
import '../base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'settings.g.dart';

@JsonSerializable()
class Settings extends BaseSerializable{

  bool active;
  DateTime time;
  int limit;
  List<String> catalog;

  Settings({this.active, this.time, this.limit, this.catalog});


  factory Settings.fromJson(Map<String, dynamic>json) => _$SettingsFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$SettingsToJson(this);

}