import 'package:json_annotation/json_annotation.dart';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:useful_utility/useful_utility.dart';
import '../base_serializable.dart';
import 'menu.dart';
import '../classes.dart';
import 'package:useful_utility/useful_utility.dart';

part 'carryout_order.g.dart';

@JsonSerializable()
class CarryoutOrder extends BaseSerializable{

  factory CarryoutOrder.fromJson(Map<String, dynamic> json) =>
      _$CarryoutOrderFromJson(json);

  Map<String, dynamic> toJson() => _$CarryoutOrderToJson(this);

  ///購買品項
  String name;

  ///必要選項
  List<MenuRequireOrderItem> require = List<MenuRequireOrderItem>();

  ///額外選擇
  List<MenuOptional> optional = List<MenuOptional>();

  ///數量
  int num = 1;

  ///商品價格
  double price;

  /// 備註
  String remark;

  /// Menu資料
  Menu menu;

  ///this is used on the app orders. it will store requiring data selection by users.
  /// see also: [CarryoutMenuOptionsPage]
  Map<String, MenuOptional> requireTemp = Map<String, MenuOptional>();

  CarryoutOrder({
    @required this.name,
    List<MenuRequireOrderItem> require,
    List<MenuOptional> optional,
    this.num = 1,
    this.price = 0.0,
    this.remark,
    Map<String, MenuOptional> requireTemp,
    Menu menu,
  }) {
    assert(name != null);
    this.require = require ?? List<MenuRequireOrderItem>();
    this.optional = optional ?? List<MenuOptional>();
    this.requireTemp = requireTemp ?? Map<String, MenuOptional>();
    this.menu = menu;
  }



  factory CarryoutOrder.copyWith(CarryoutOrder other) {
    return new CarryoutOrder(
      name: other.name,
      require: List<MenuRequireOrderItem>.from(other.require),
      optional: List<MenuOptional>.from(other.optional),
      num: other.num,
      price: other.price,
      remark: other.remark,
      requireTemp: Map<String, MenuOptional>.from(other.requireTemp),
      menu: other.menu,
    );
  }



  /// store data to the require list
  List<MenuRequireOrderItem> requiredTempToRequire() {
    require.clear();
    requireTemp.forEach((String key, MenuOptional opt) {
      debug('key $key');
      require.add(MenuRequireOrderItem(name: key, option: opt));
    });
    return require;
  }

  double get subtotal {
    // item price plus optionals times num
    double optSubtotal = 0.0;
    if (optional != null) {
      optional.forEach((element) {
        optSubtotal += element.price;
      });
    }
    return (optSubtotal + price) * num;
  }
}
