import '../classes.dart';
import 'package:json_annotation/json_annotation.dart';
import '../base_serializable.dart';

part 'menu.g.dart';

@JsonSerializable()
class Menu extends BaseSerializable {
  String catalog;
  String name;
  String description;
  ImageInfo img;
  List<MenuRequireItem> require;
  List<MenuRequireItem> optional;
  String storeUid;
  bool active;
  num price;

  Menu(
      {this.catalog,
      this.name,
      this.description,
      this.img,
      this.require,
      this.optional,
      this.storeUid,
      this.active,
      this.price});

  factory Menu.fromJson(Map<String, dynamic> json) => _$MenuFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MenuToJson(this);
}

///菜單選項必填項目
@JsonSerializable()
class MenuRequireItem extends BaseSerializable {
  String name;
  List<MenuOptional> option;

  MenuRequireItem({this.name, this.option});

  factory MenuRequireItem.fromJson(Map<String, dynamic> json) =>
      _$MenuRequireItemFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MenuRequireItemToJson(this);
}

///菜單選項必填項目
@JsonSerializable()
class MenuRequireOrderItem extends BaseSerializable {
  String name;
//  num price;
  MenuOptional option;

  MenuRequireOrderItem({this.name, this.option});

  factory MenuRequireOrderItem.fromJson(Map<String, dynamic> json) =>
      _$MenuRequireOrderItemFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MenuRequireOrderItemToJson(this);
}

/// Option model
@JsonSerializable()
class MenuOptional extends BaseSerializable {
  /// 標題
  String name;

  /// 價格
  num price;

  MenuOptional({this.name, this.price});

  factory MenuOptional.fromJson(Map<String, dynamic> json) =>
      _$MenuOptionalFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$MenuOptionalToJson(this);
}
