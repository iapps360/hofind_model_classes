// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Menu _$MenuFromJson(Map<String, dynamic> json) {
  return Menu(
    catalog: json['catalog'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    img: json['img'] == null
        ? null
        : ImageInfo.fromJson(json['img'] as Map<String, dynamic>),
    require: (json['require'] as List)
        ?.map((e) => e == null
            ? null
            : MenuRequireItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    optional: (json['optional'] as List)
        ?.map((e) => e == null
            ? null
            : MenuRequireItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    storeUid: json['storeUid'] as String,
    active: json['active'] as bool,
    price: json['price'] as num,
  );
}

Map<String, dynamic> _$MenuToJson(Menu instance) => <String, dynamic>{
      'catalog': instance.catalog,
      'name': instance.name,
      'description': instance.description,
      'img': instance.img,
      'require': instance.require,
      'optional': instance.optional,
      'storeUid': instance.storeUid,
      'active': instance.active,
      'price': instance.price,
    };

MenuRequireItem _$MenuRequireItemFromJson(Map<String, dynamic> json) {
  return MenuRequireItem(
    name: json['name'] as String,
    option: (json['option'] as List)
        ?.map((e) =>
            e == null ? null : MenuOptional.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$MenuRequireItemToJson(MenuRequireItem instance) =>
    <String, dynamic>{
      'name': instance.name,
      'option': instance.option,
    };

MenuRequireOrderItem _$MenuRequireOrderItemFromJson(Map<String, dynamic> json) {
  return MenuRequireOrderItem(
    name: json['name'] as String,
    option: json['option'] == null
        ? null
        : MenuOptional.fromJson(json['option'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MenuRequireOrderItemToJson(
        MenuRequireOrderItem instance) =>
    <String, dynamic>{
      'name': instance.name,
      'option': instance.option,
    };

MenuOptional _$MenuOptionalFromJson(Map<String, dynamic> json) {
  return MenuOptional(
    name: json['name'] as String,
    price: json['price'] as num,
  );
}

Map<String, dynamic> _$MenuOptionalToJson(MenuOptional instance) =>
    <String, dynamic>{
      'name': instance.name,
      'price': instance.price,
    };
