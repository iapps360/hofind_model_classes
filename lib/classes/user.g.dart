// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    uid: json['uid'] as String,
    id: json['id'] as String,
    phone: json['phone'] == null
        ? null
        : PhoneNumber.fromJson(json['phone'] as Map<String, dynamic>),
    email: json['email'] as String,
    name: json['name'] as String,
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    gender: json['gender'] as String,
    birthday: json['birthday'] == null
        ? null
        : DateTime.parse(json['birthday'] as String),
    post: json['post'] as String,
    contry: json['contry'] as String,
    city: json['city'] as String,
    district: json['district'] as String,
    address: json['address'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    img: json['img'] == null
        ? null
        : Img.fromJson(json['img'] as Map<String, dynamic>),
  )
    ..bindDate = json['bindDate'] == null
        ? null
        : DateTime.parse(json['bindDate'] as String)
    ..invite = json['invite'] == null ? null : Invite.fromJson(json['invite'])
    ..credit = json['credit'] == null
        ? null
        : CreditCard.fromJson(json['credit'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'uid': instance.uid,
      'id': instance.id,
      'phone': instance.phone,
      'email': instance.email,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'name': instance.name,
      'gender': instance.gender,
      'birthday': instance.birthday?.toIso8601String(),
      'post': instance.post,
      'contry': instance.contry,
      'city': instance.city,
      'district': instance.district,
      'address': instance.address,
      'bindDate': instance.bindDate?.toIso8601String(),
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'invite': instance.invite,
      'img': instance.img,
      'credit': instance.credit,
    };

Invite _$InviteFromJson(Map<String, dynamic> json) {
  return Invite(
    uid: json['uid'] as String,
    type: json['type'] as String,
    rate: json['rate'] as num,
  );
}

Map<String, dynamic> _$InviteToJson(Invite instance) => <String, dynamic>{
      'uid': instance.uid,
      'type': instance.type,
      'rate': instance.rate,
    };
