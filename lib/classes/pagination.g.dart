// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pagination _$PaginationFromJson(Map<String, dynamic> json) {
  return Pagination(
    index: json['index'] as int,
    limit: json['limit'] as int,
    total: json['total'] as int,
    size: json['size'] as int,
  );
}

Map<String, dynamic> _$PaginationToJson(Pagination instance) =>
    <String, dynamic>{
      'index': instance.index,
      'limit': instance.limit,
      'total': instance.total,
      'size': instance.size,
    };
