// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'img.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Img _$ImgFromJson(Map<String, dynamic> json) {
  return Img(
    uid: json['uid'] as String,
    small: json['small'] as String,
    medium: json['medium'] as String,
    large: json['large'] as String,
  );
}

Map<String, dynamic> _$ImgToJson(Img instance) => <String, dynamic>{
      'uid': instance.uid,
      'small': instance.small,
      'medium': instance.medium,
      'large': instance.large,
    };
