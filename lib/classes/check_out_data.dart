import 'classes.dart';
import "package:json_annotation/json_annotation.dart";
import 'base_serializable.dart';

import 'store_detail.dart';

part 'check_out_data.g.dart';

@JsonSerializable()
class Checkout extends BaseSerializable{
  StoreDetail store;
  HoWalletAmount user;
  CreditCard credit;


  Checkout({this.store, this.user, this.credit});
  factory Checkout.fromJson(Map<String, dynamic>json) => _$CheckoutFromJson(json);

  @override
  Map<String, dynamic>  toJson() => _$CheckoutToJson(this);
}

