/// this is a model to convert the "使用者 餘額" to object
import "package:json_annotation/json_annotation.dart";
import "base_serializable.dart";

part 'ho_wallet_amont.g.dart';

@JsonSerializable()
class HoWalletAmount extends BaseSerializable{

  double coin;
  double point;

  HoWalletAmount({this.coin, this.point});

  factory HoWalletAmount.fromJson(Map<String, dynamic>json) => _$HoWalletAmountFromJson(json);

  @override
  Map<String, dynamic>  toJson() => _$HoWalletAmountToJson(this);


}