// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StoreDetail _$StoreDetailFromJson(Map<String, dynamic> json) {
  return StoreDetail(
    active: json['active'] as bool,
    address: json['address'] as String,
    agentUid: json['agentUid'] as String,
    bank: json['bank'] == null
        ? null
        : Bank.fromJson(json['bank'] as Map<String, dynamic>),
    catalog: json['catalog'] as String,
    contact: json['contact'] == null
        ? null
        : Contact.fromJson(json['contact'] as Map<String, dynamic>),
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    credit: json['credit'] == null
        ? null
        : Credit.fromJson(json['credit'] as Map<String, dynamic>),
    currency: json['currency'] as String,
    logo: json['logo'] == null
        ? null
        : ImageInfo.fromJson(json['logo'] as Map<String, dynamic>),
    imgList: (json['imgList'] as List)
        ?.map((e) =>
            e == null ? null : ImageInfo.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    invoice: json['invoice'] == null
        ? null
        : Invoice.fromJson(json['invoice'] as Map<String, dynamic>),
    level: json['level'] as String,
    location: json['location'] == null
        ? null
        : Location.fromJson(json['location'] as Map<String, dynamic>),
    menu: json['menu'] as List,
    name: json['name'] as String,
    openTime: (json['openTime'] as List)
        ?.map((e) =>
            e == null ? null : OpenTime.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    owner: json['owner'] == null
        ? null
        : Owner.fromJson(json['owner'] as Map<String, dynamic>),
    refund: (json['refund'] as num)?.toDouble(),
    tag: (json['tag'] as List)?.map((e) => e as String)?.toList(),
    telephone: json['telephone'] as String,
    uid: json['uid'] as String,
    discountDirect: (json['discountDirect'] as num)?.toDouble(),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    comment: json['comment'] == null
        ? null
        : Comment.fromJson(json['comment'] as Map<String, dynamic>),
    indicate: json['indicate'] == null
        ? null
        : Indicate.fromJson(json['indicate'] as Map<String, dynamic>),
    remark: json['remark'] as String,
    introduction: json['introduction'] as String,
    socialURL: json['link'] == null
        ? null
        : SocialURL.fromJson(json['link'] as Map<String, dynamic>),
    political: json['political'] as String,
    post: json['post'] as String,
    firstRefund: (json['firstRefund'] as num)?.toDouble(),
    secondRefund: (json['secondRefund'] as num)?.toDouble(),
    areaRefund: (json['areaRefund'] as num)?.toDouble(),
    agentRefund: (json['agentRefund'] as num)?.toDouble(),
    areaUid: json['areaUid'] as String,
    takeoutTime: json['takeoutTime'] as String,
  )..menuList = (json['menuList'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(
          k,
          (e as List)
              ?.map((e) =>
                  e == null ? null : Menu.fromJson(e as Map<String, dynamic>))
              ?.toList()),
    );
}

Map<String, dynamic> _$StoreDetailToJson(StoreDetail instance) =>
    <String, dynamic>{
      'active': instance.active,
      'address': instance.address,
      'agentUid': instance.agentUid,
      'bank': instance.bank,
      'catalog': instance.catalog,
      'contact': instance.contact,
      'createdAt': instance.createdAt?.toIso8601String(),
      'credit': instance.credit,
      'currency': instance.currency,
      'logo': instance.logo,
      'imgList': instance.imgList,
      'invoice': instance.invoice,
      'level': instance.level,
      'location': instance.location,
      'menu': instance.menu,
      'name': instance.name,
      'openTime': instance.openTime,
      'owner': instance.owner,
      'refund': instance.refund,
      'tag': instance.tag,
      'telephone': instance.telephone,
      'uid': instance.uid,
      'discountDirect': instance.discountDirect,
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'comment': instance.comment,
      'indicate': instance.indicate,
      'remark': instance.remark,
      'introduction': instance.introduction,
      'political': instance.political,
      'post': instance.post,
      'firstRefund': instance.firstRefund,
      'secondRefund': instance.secondRefund,
      'areaRefund': instance.areaRefund,
      'agentRefund': instance.agentRefund,
      'areaUid': instance.areaUid,
      'menuList': instance.menuList,
      'takeoutTime': instance.takeoutTime,
      'link': instance.socialURL,
    };

Bank _$BankFromJson(Map<String, dynamic> json) {
  return Bank(
    account: json['account'] as String,
    code: json['code'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$BankToJson(Bank instance) => <String, dynamic>{
      'account': instance.account,
      'code': instance.code,
      'name': instance.name,
    };

Contact _$ContactFromJson(Map<String, dynamic> json) {
  return Contact(
    mail: json['mail'] as String,
    name: json['name'] as String,
    telephone: json['telephone'] as String,
  );
}

Map<String, dynamic> _$ContactToJson(Contact instance) => <String, dynamic>{
      'name': instance.name,
      'telephone': instance.telephone,
      'mail': instance.mail,
    };

Credit _$CreditFromJson(Map<String, dynamic> json) {
  return Credit(
    type: json['type'] as String,
    no: json['no'] as String,
    expired: json['expired'] == null
        ? null
        : DateTime.parse(json['expired'] as String),
  );
}

Map<String, dynamic> _$CreditToJson(Credit instance) => <String, dynamic>{
      'no': instance.no,
      'expired': instance.expired?.toIso8601String(),
      'type': instance.type,
    };

Invoice _$InvoiceFromJson(Map<String, dynamic> json) {
  return Invoice(
    type: json['type'] as String,
  );
}

Map<String, dynamic> _$InvoiceToJson(Invoice instance) => <String, dynamic>{
      'type': instance.type,
    };

Owner _$OwnerFromJson(Map<String, dynamic> json) {
  return Owner(
    iso: json['iso'] as String,
    mail: json['mail'] as String,
    address: json['address'] as String,
    id: json['id'] as String,
    name: json['name'] as String,
    telephone: json['telephone'] as String,
  );
}

Map<String, dynamic> _$OwnerToJson(Owner instance) => <String, dynamic>{
      'iso': instance.iso,
      'mail': instance.mail,
      'address': instance.address,
      'id': instance.id,
      'name': instance.name,
      'telephone': instance.telephone,
    };

Indicate _$IndicateFromJson(Map<String, dynamic> json) {
  return Indicate(
    show: json['show'] as bool,
    wine: json['wine'] as bool,
    linkFb: json['linkFb'] as bool,
    linkOffical: json['linkOffical'] as bool,
    linkIg: json['linkIg'] as bool,
    takeout: json['takeout'] as bool,
    top: json['top'] as bool,
  );
}

Map<String, dynamic> _$IndicateToJson(Indicate instance) => <String, dynamic>{
      'wine': instance.wine,
      'linkFb': instance.linkFb,
      'linkOffical': instance.linkOffical,
      'linkIg': instance.linkIg,
      'takeout': instance.takeout,
      'top': instance.top,
      'show': instance.show,
    };
