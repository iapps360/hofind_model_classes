// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ho_coin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HoCoin _$HoCoinFromJson(Map<String, dynamic> json) {
  return HoCoin(
    service: json['service'] as String,
    recordUid: json['recordUid'] as String,
    userUid: json['userUid'] as String,
    type: json['type'] as int,
    deposit: (json['deposit'] as num)?.toDouble(),
    withdrawal: (json['withdrawal'] as num)?.toDouble(),
    remark: json['remark'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updateAt: json['updateAt'] == null
        ? null
        : DateTime.parse(json['updateAt'] as String),
    uid: json['uid'] as String,
  );
}

Map<String, dynamic> _$HoCoinToJson(HoCoin instance) => <String, dynamic>{
      'service': instance.service,
      'recordUid': instance.recordUid,
      'userUid': instance.userUid,
      'type': instance.type,
      'deposit': instance.deposit,
      'withdrawal': instance.withdrawal,
      'remark': instance.remark,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updateAt': instance.updateAt?.toIso8601String(),
      'uid': instance.uid,
    };

HoPoint _$HoPointFromJson(Map<String, dynamic> json) {
  return HoPoint(
    service: json['service'] as String,
    recordUid: json['recordUid'] as String,
    userUid: json['userUid'] as String,
    type: json['type'] as int,
    deposit: (json['deposit'] as num)?.toDouble(),
    withdrawal: (json['withdrawal'] as num)?.toDouble(),
    remark: json['remark'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updateAt: json['updateAt'] == null
        ? null
        : DateTime.parse(json['updateAt'] as String),
    uid: json['uid'] as String,
  );
}

Map<String, dynamic> _$HoPointToJson(HoPoint instance) => <String, dynamic>{
      'service': instance.service,
      'recordUid': instance.recordUid,
      'userUid': instance.userUid,
      'type': instance.type,
      'deposit': instance.deposit,
      'withdrawal': instance.withdrawal,
      'remark': instance.remark,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updateAt': instance.updateAt?.toIso8601String(),
      'uid': instance.uid,
    };
