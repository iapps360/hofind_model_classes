// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prize_gift_record.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrizeGiftRecord _$PrizeGiftRecordFromJson(Map<String, dynamic> json) {
  return PrizeGiftRecord(
    state: json['state'] as String,
    name: json['name'] as String,
    type: json['type'] as String,
    userUid: json['userUid'] as String,
    refUid: json['refUid'] as String,
    uid: json['uid'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
    val: json['val'] as num,
    img: json['img'] == null
        ? null
        : Img.fromJson(json['img'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PrizeGiftRecordToJson(PrizeGiftRecord instance) =>
    <String, dynamic>{
      'state': instance.state,
      'name': instance.name,
      'type': instance.type,
      'userUid': instance.userUid,
      'refUid': instance.refUid,
      'uid': instance.uid,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'val': instance.val,
      'img': instance.img,
    };
