class HofindError implements Exception {
  static const Map<String, String> _codes = {
  'checkout.store'                       :  '查無此店家',
  'tradingRecord.processing'             :  '尚有一筆單未完結',
  'tradingRecord.coin'                   :  '好康幣不足',
  'tradingRecord.store'                  :  '查無此店家',
  'tradingRecord.credit.bind'            :  '信用卡尚未綁定',
  'tradingRecord.credit.error'           :  '信用卡有問題',
  'billing.storeCoin.processing'         :  '尚有一筆單未完結',
  'billing.storeCoin.coin'               :  '好康幣不足',
  'billing.storeCoin.store'              :  '查無此店家',
  'manage.user'                          :  '該使用者已存在',
  'func.user.bind.uid'                   :  '消費者不能綁定自己',
  'func.user.bind.introduce'             :  '消費者已被綁定過',
  'prize.gift.point'                     :  '紅利點數不足',
  'prize.gift.user'                      :  '消費者不存在',
  'prize.wheel.result.tradingRecordUid'  :  '查無此單號',
  'prize.wheel.result'                   :  '此交易訂單已使用過，不能再抽獎',
  'coin.coin'                            :  '好康幣餘額不足',
  'storeCoin.coin'                       :  '好康幣餘額不足',
  'tag.name'                             :  '已存在子項目',
  'takeoutSetting.store.takeoff'         :  '店家無開啟外帶功能',
  'store.menu.takeout'                   :  '店家無開啟外帶功能',
  'menu.store'                           :  '查無此店家',
  'menu.store.takeoff'                   :  '店家無開啟外帶功能',
  'admin.account'                        :  '已有存在的帳號',
  'area.id'                              :  '已存在此id',
  'employee.id'                          :  '已存在此id',
  'group.id'                             :  '已存在此id',
  'auth.phone'                           :  '手機不正確',
  'auth.phone.login.validCode'           :  '驗證碼不正確',
  'user.phone'                           :  '手機不正確',
  };

  /// error code to message in Chinese
  String toMessage(String errorCode){
    if(_codes.containsKey(errorCode)){
      return _codes[errorCode];
    }else{
      return '發生未知錯誤($errorCode)';
    }
  }

  final int status;
  final String msg;
  final String type;
  final List<dynamic> errors;

  const HofindError({this.status, this.msg, this.type, this.errors});

 @override
  String toString() {

    return {
      'status': status,
      'msg': msg,
      'type': toMessage(type),
      'errors': errors
    }.toString();
  }
}

