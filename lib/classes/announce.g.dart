// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'announce.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Announce _$AnnounceFromJson(Map<String, dynamic> json) {
  return Announce(
    img: json['img'] == null
        ? null
        : ImageInfo.fromJson(json['img'] as Map<String, dynamic>),
    title: json['title'] as String,
    content: json['content'] as String,
    type: json['type'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uid: json['uid'] as String,
    linkFlag: json['linkFlag'] as bool,
    link: json['link'] as String,
  );
}

Map<String, dynamic> _$AnnounceToJson(Announce instance) => <String, dynamic>{
      'img': instance.img,
      'title': instance.title,
      'content': instance.content,
      'type': instance.type,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uid': instance.uid,
      'linkFlag': instance.linkFlag,
      'link': instance.link,
    };
