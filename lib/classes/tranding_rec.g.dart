// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tranding_rec.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrandingRec _$TrandingRecFromJson(Map<String, dynamic> json) {
  return TrandingRec(
    identifyNo: json['identifyNo'] as String,
    discount: json['discount'] == null
        ? null
        : Discount.fromJson(json['discount'] as Map<String, dynamic>),
    amount: json['amount'] == null
        ? null
        : Amount.fromJson(json['amount'] as Map<String, dynamic>),
    userUid: json['userUid'] as String,
    storeUid: json['storeUid'] as String,
    areaUid: json['areaUid'] as String,
    state: json['state'] as String,
  );
}

Map<String, dynamic> _$TrandingRecToJson(TrandingRec instance) =>
    <String, dynamic>{
      'identifyNo': instance.identifyNo,
      'discount': instance.discount,
      'amount': instance.amount,
      'userUid': instance.userUid,
      'storeUid': instance.storeUid,
      'areaUid': instance.areaUid,
      'state': instance.state,
    };

Discount _$DiscountFromJson(Map<String, dynamic> json) {
  return Discount(
    coin: (json['coin'] as num)?.toDouble(),
    direct: (json['direct'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$DiscountToJson(Discount instance) => <String, dynamic>{
      'coin': instance.coin,
      'direct': instance.direct,
    };

Amount _$AmountFromJson(Map<String, dynamic> json) {
  return Amount(
    total: (json['total'] as num)?.toDouble(),
    pay: (json['pay'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$AmountToJson(Amount instance) => <String, dynamic>{
      'total': instance.total,
      'pay': instance.pay,
    };
