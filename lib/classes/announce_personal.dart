import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'announce_personal.g.dart';

@JsonSerializable()
class AnnouncePersonal extends BaseSerializable{


  int      type;
  bool      read;
  String      userUid;
  String      title;
  String      content;
  DateTime      createdAt;
  DateTime      updatedAt;
  String      uid;



  AnnouncePersonal({
    this.type,
    this.read,
    this.userUid,
    this.title,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.uid,
  });

  factory AnnouncePersonal.fromJson(Map<String, dynamic>json) => _$AnnouncePersonalFromJson(json);

  @override
  Map<String, dynamic> toJson() {
    return _$AnnouncePersonalToJson(this);
  }

}