import 'img.dart';
import 'package:json_annotation/json_annotation.dart';
import 'phone_number.dart';
import 'package:intl/intl.dart';

part 'userComment.g.dart';

@JsonSerializable()
class UserComment {
  PhoneNumber phone;
  String userUid;
  String storeUid;
  int starNum;
  String comment;
  String uid;
  String createdAt;
  Img img;
  String _phoneSecure;
  bool everEdit;

  UserComment(
      {this.phone,
      this.userUid,
      this.storeUid,
      this.starNum,
      this.comment,
      this.uid,
      this.createdAt,
      this.img,
      this.everEdit});

  String phoneSecure({int secureText=3}) {
    if (_phoneSecure == null) {
      if (phone.national != null) {
        _phoneSecure = phone.national.replaceRange(
            phone.national.length - secureText, phone.national.length, '*' * secureText);
      } else {
        if (phone.full != null) {
          _phoneSecure = phone.full
              .replaceRange(phone.full.length - secureText, phone.full.length, '*' * secureText);
        } else {
          _phoneSecure = "";
        }
      }
      return _phoneSecure;
    }
    return _phoneSecure;
  }

  getCreatedTimeStr() {
    try {
      //提交時間
      var timeStr = DateFormat("yyyy/MM/dd") //yyyy/MM/dd HH:mm
          .format(DateTime.parse(this.createdAt).toLocal());
      return timeStr;
    } catch (e) {
      return "";
    }
  }

  factory UserComment.fromJson(Map<String, dynamic> json) =>
      _$UserCommentFromJson(json);
  Map<String, dynamic> toJson() => _$UserCommentToJson(this);
}
