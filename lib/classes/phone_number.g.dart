// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_number.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhoneNumber _$PhoneNumberFromJson(Map<String, dynamic> json) {
  return PhoneNumber(
    full: json['full'] as String,
    iso: json['iso'] as String,
    code: json['code'] as String,
    number: json['number'] as String,
    national: json['national'] as String,
  );
}

Map<String, dynamic> _$PhoneNumberToJson(PhoneNumber instance) =>
    <String, dynamic>{
      'full': instance.full,
      'iso': instance.iso,
      'code': instance.code,
      'number': instance.number,
      'national': instance.national,
    };
