import 'dart:core';
import 'package:json_annotation/json_annotation.dart';
import 'base_serializable.dart';

part 'tranding_rec.g.dart';

@JsonSerializable()
class TrandingRec extends BaseSerializable {
  String identifyNo;
  Discount discount;
  Amount amount;
  String userUid;
  String storeUid;
  String areaUid;
  String state;

  TrandingRec({
    this.identifyNo,
    this.discount,
    this.amount,
    this.userUid,
    this.storeUid,
    this.areaUid,
    this.state,
  });

  factory TrandingRec.fromJson(Map<String, dynamic> json) =>
      _$TrandingRecFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$TrandingRecToJson(this);
}

@JsonSerializable()
class Discount extends BaseSerializable {
  double coin;
  double direct;

  Discount({this.coin, this.direct});

  factory Discount.fromJson(Map<String, dynamic> json) =>
      _$DiscountFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DiscountToJson(this);

}

@JsonSerializable()
class Amount extends BaseSerializable {
  double total;
  double pay;

  Amount({
    this.total,
    this.pay,
  });

  factory Amount.fromJson(Map<String, dynamic> json) => _$AmountFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AmountToJson(this);

}
