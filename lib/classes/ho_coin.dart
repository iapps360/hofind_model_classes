/// this is a model to convert the "使用者 餘額" to object
import "package:json_annotation/json_annotation.dart";
import "base_serializable.dart";

part 'ho_coin.g.dart';

@JsonSerializable()
class HoCoin extends BaseSerializable {
  String service;
  String recordUid;
  String userUid;
  int type;
  double deposit;
  double withdrawal;
  String remark;
  DateTime createdAt;
  DateTime updateAt;
  String uid;

  HoCoin({
    this.service,
    this.recordUid,
    this.userUid,
    this.type,
    this.deposit,
    this.withdrawal,
    this.remark,
    this.createdAt,
    this.updateAt,
    this.uid
  });

  factory HoCoin.fromJson(Map<String, dynamic> json) {
    HoCoin coin = _$HoCoinFromJson(json);
    return coin;
  }

  @override
  Map<String, dynamic> toJson() => _$HoCoinToJson(this);
}

@JsonSerializable()
class HoPoint extends BaseSerializable {
  String service;
  String recordUid;
  String userUid;
  int type;
  double deposit;
  double withdrawal;
  String remark;
  DateTime createdAt;
  DateTime updateAt;
  String uid;

  HoPoint({
    this.service,
    this.recordUid,
    this.userUid,
    this.type,
    this.deposit,
    this.withdrawal,
    this.remark,
    this.createdAt,
    this.updateAt,
    this.uid
  });

  factory HoPoint.fromJson(Map<String, dynamic> json) {
    HoPoint point = _$HoPointFromJson(json);
    return point;
  }

  @override
  Map<String, dynamic> toJson() => _$HoPointToJson(this);

}