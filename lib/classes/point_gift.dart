import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'classes.dart';


part 'point_gift.g.dart';
@JsonSerializable()
class PointGift extends BaseSerializable {
  final _UseDate useDate;
  final String type;
  final int qty;
  final ImageInfo img;
  final String name;
  final int point;
  final String content;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String uid;

  PointGift({
    this.useDate,
    this.type,
    this.qty,
    this.img,
    this.name,
    this.point,
    this.content,
    this.createdAt,
    this.updatedAt,
    this.uid,
  });

  factory PointGift.fromJson(Map<String, dynamic> json) =>  _$PointGiftFromJson(json);

  @override
  Map<String, dynamic> toJson() {
    return _$PointGiftToJson(this);
  }
}

@JsonSerializable()
class _UseDate extends BaseSerializable {
  final DateTime start;
  final DateTime end;

  _UseDate({this.start, this.end});

  factory _UseDate.fromJson(Map<String, dynamic> json) =>  _$_UseDateFromJson(json);

  @override
  Map<String, dynamic> toJson() {
    return _$_UseDateToJson(this);
  }
}
