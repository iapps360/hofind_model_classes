// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gift_rec.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GiftRec _$GiftRecFromJson(Map<String, dynamic> json) {
  return GiftRec(
    name: json['name'] as String,
    type: json['type'] as String,
    img: json['img'] == null
        ? null
        : ImageInfo.fromJson(json['img'] as Map<String, dynamic>),
    val: json['val'] as String,
  )
    ..state = json['state'] as String
    ..content = json['content'] as String;
}

Map<String, dynamic> _$GiftRecToJson(GiftRec instance) => <String, dynamic>{
      'state': instance.state,
      'name': instance.name,
      'type': instance.type,
      'img': instance.img,
      'val': instance.val,
      'content': instance.content,
    };
