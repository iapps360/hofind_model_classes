// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ho_wallet_amont.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HoWalletAmount _$HoWalletAmountFromJson(Map<String, dynamic> json) {
  return HoWalletAmount(
    coin: (json['coin'] as num)?.toDouble(),
    point: (json['point'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$HoWalletAmountToJson(HoWalletAmount instance) =>
    <String, dynamic>{
      'coin': instance.coin,
      'point': instance.point,
    };
