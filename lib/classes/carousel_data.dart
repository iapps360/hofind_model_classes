import 'classes.dart';
import "package:json_annotation/json_annotation.dart";
import 'base_serializable.dart';

part 'carousel_data.g.dart';

@JsonSerializable()
class CarouselData extends BaseSerializable{

  /// the type is for next action
  /// type:
  ///     none => nothing to do
  ///     store => show store detail page
  ///     link => open web page
  ///     event => going to event page
  String type;
  ImageInfo img;
  String storeUid;
  String link;
  String eventUid;

  CarouselData({this.type, this.img, this.storeUid, this.link, this.eventUid});

  factory CarouselData.fromJson(Map<String, dynamic>json) => _$CarouselDataFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CarouselDataToJson(this);
}