// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userComment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserComment _$UserCommentFromJson(Map<String, dynamic> json) {
  return UserComment(
    phone: json['phone'] == null
        ? null
        : PhoneNumber.fromJson(json['phone'] as Map<String, dynamic>),
    userUid: json['userUid'] as String,
    storeUid: json['storeUid'] as String,
    starNum: json['starNum'] as int,
    comment: json['comment'] as String,
    uid: json['uid'] as String,
    createdAt: json['createdAt'] as String,
    img: json['img'] == null
        ? null
        : Img.fromJson(json['img'] as Map<String, dynamic>),
    everEdit: json['everEdit'] as bool,
  );
}

Map<String, dynamic> _$UserCommentToJson(UserComment instance) =>
    <String, dynamic>{
      'phone': instance.phone,
      'userUid': instance.userUid,
      'storeUid': instance.storeUid,
      'starNum': instance.starNum,
      'comment': instance.comment,
      'uid': instance.uid,
      'createdAt': instance.createdAt,
      'img': instance.img,
      'everEdit': instance.everEdit,
    };
