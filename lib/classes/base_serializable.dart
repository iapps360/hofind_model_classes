import 'dart:convert';
import 'dart:core';

abstract class BaseSerializable extends Object {

  Map<String, dynamic> toJson();

  @override
  String toString() {
    Map<String, dynamic> map = toJson();
    map.removeWhere((k,v) => v==null);
    return json.encode(map);
  }
}