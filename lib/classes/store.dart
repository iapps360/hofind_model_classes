import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';

part "store.g.dart";

@JsonSerializable()
class Store extends BaseSerializable {
  String uid;
  String name;
  String catalog;
  List<String> tag;
  String address;
  String telephone;
  Location location;
  List<OpenTime> openTime;
  List<dynamic> menu;
  ImageInfo logo;
  double discountDirect;
  Comment comment;
  bool active;
  bool takeoutFlag;

  Store(
      {this.uid,
      this.name,
      this.catalog,
      this.tag,
      this.address,
      this.telephone,
      this.openTime,
      this.menu,
      this.logo,
      this.discountDirect,
      this.comment,
      this.active,
      this.takeoutFlag});

  factory Store.fromJson(Map<String, dynamic> json) => _$StoreFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$StoreToJson(this);

  static List<Store> fromList(List list) {
    return List<Store>.from(list.map((e) => Store.fromJson(e)));
  }
}

@JsonSerializable()
class OpenTime extends BaseSerializable {
  /// Is active at this date
  bool active;

  /// a time array, it will show open time ranges in a days
  List<_Times> data;

  OpenTime({this.active, this.data});

  factory OpenTime.fromJson(Map<String, dynamic> json) =>
      _$OpenTimeFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$OpenTimeToJson(this);
}

@JsonSerializable()
class _Times extends BaseSerializable {
  String start;
  String end;

  _Times({this.start, this.end});

  factory _Times.fromJson(Map<String, dynamic> json) => _$_TimesFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$_TimesToJson(this);
}

@JsonSerializable()
class Location extends BaseSerializable {
  String type;
  List<double> coordinates;

  Location({this.type, this.coordinates});

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$LocationToJson(this);
}

@JsonSerializable()
class ImageInfo extends BaseSerializable {
  List path;
  String uid;
  String small;
  String medium;
  String large;

  ImageInfo({this.uid, this.small, this.medium, this.large, this.path});

  factory ImageInfo.fromJson(Map<String, dynamic> json) =>
      _$ImageInfoFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$ImageInfoToJson(this);
}

@JsonSerializable()
class Comment extends BaseSerializable {
  num score;
  num count;
  num averageScore;

  Comment({this.score, this.count, this.averageScore});

  factory Comment.fromJson(Map<String, dynamic> json) =>
      _$CommentFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CommentToJson(this);
}
