import 'base_serializable.dart';
import 'store.dart';
import 'package:json_annotation/json_annotation.dart';

part 'announce.g.dart';

@JsonSerializable()
class Announce extends BaseSerializable{

  ImageInfo       img;
  String          title;
  String          content;
  String          type;
  DateTime        createdAt;
  DateTime        updatedAt;
  String          uid;
  bool            linkFlag;
  String          link;

  Announce({
    this.img,
    this.title,
    this.content,
    this.type,
    this.createdAt,
    this.updatedAt,
    this.uid,
    this.linkFlag,
    this.link
  });

  factory Announce.fromJson(Map<String, dynamic>json) => _$AnnounceFromJson(json);

  @override
  Map<String, dynamic> toJson() {
    return _$AnnounceToJson(this);
  }

}