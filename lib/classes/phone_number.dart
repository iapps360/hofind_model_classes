import 'base_serializable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'phone_number.g.dart';

@JsonSerializable()
class PhoneNumber extends BaseSerializable {
  String full; //+886number
  String iso; // TW
  String code; //886
  String number; // only phone number without 886
  String national;

  PhoneNumber({this.full, this.iso, this.code, this.number, this.national});

  factory PhoneNumber.fromJson(Map<String, dynamic> json) =>
      _$PhoneNumberFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$PhoneNumberToJson(this);
}
