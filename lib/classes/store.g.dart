// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Store _$StoreFromJson(Map<String, dynamic> json) {
  return Store(
    uid: json['uid'] as String,
    name: json['name'] as String,
    catalog: json['catalog'] as String,
    tag: (json['tag'] as List)?.map((e) => e as String)?.toList(),
    address: json['address'] as String,
    telephone: json['telephone'] as String,
    openTime: (json['openTime'] as List)
        ?.map((e) =>
            e == null ? null : OpenTime.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    menu: json['menu'] as List,
    logo: json['logo'] == null
        ? null
        : ImageInfo.fromJson(json['logo'] as Map<String, dynamic>),
    discountDirect: (json['discountDirect'] as num)?.toDouble(),
    comment: json['comment'] == null
        ? null
        : Comment.fromJson(json['comment'] as Map<String, dynamic>),
    active: json['active'] as bool,
    takeoutFlag: json['takeoutFlag'] as bool,
  )..location = json['location'] == null
      ? null
      : Location.fromJson(json['location'] as Map<String, dynamic>);
}

Map<String, dynamic> _$StoreToJson(Store instance) => <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'catalog': instance.catalog,
      'tag': instance.tag,
      'address': instance.address,
      'telephone': instance.telephone,
      'location': instance.location,
      'openTime': instance.openTime,
      'menu': instance.menu,
      'logo': instance.logo,
      'discountDirect': instance.discountDirect,
      'comment': instance.comment,
      'active': instance.active,
      'takeoutFlag': instance.takeoutFlag,
    };

OpenTime _$OpenTimeFromJson(Map<String, dynamic> json) {
  return OpenTime(
    active: json['active'] as bool,
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : _Times.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$OpenTimeToJson(OpenTime instance) => <String, dynamic>{
      'active': instance.active,
      'data': instance.data,
    };

_Times _$_TimesFromJson(Map<String, dynamic> json) {
  return _Times(
    start: json['start'] as String,
    end: json['end'] as String,
  );
}

Map<String, dynamic> _$_TimesToJson(_Times instance) => <String, dynamic>{
      'start': instance.start,
      'end': instance.end,
    };

Location _$LocationFromJson(Map<String, dynamic> json) {
  return Location(
    type: json['type'] as String,
    coordinates: (json['coordinates'] as List)
        ?.map((e) => (e as num)?.toDouble())
        ?.toList(),
  );
}

Map<String, dynamic> _$LocationToJson(Location instance) => <String, dynamic>{
      'type': instance.type,
      'coordinates': instance.coordinates,
    };

ImageInfo _$ImageInfoFromJson(Map<String, dynamic> json) {
  return ImageInfo(
    uid: json['uid'] as String,
    small: json['small'] as String,
    medium: json['medium'] as String,
    large: json['large'] as String,
    path: json['path'] as List,
  );
}

Map<String, dynamic> _$ImageInfoToJson(ImageInfo instance) => <String, dynamic>{
      'path': instance.path,
      'uid': instance.uid,
      'small': instance.small,
      'medium': instance.medium,
      'large': instance.large,
    };

Comment _$CommentFromJson(Map<String, dynamic> json) {
  return Comment(
    score: json['score'] as num,
    count: json['count'] as num,
    averageScore: json['averageScore'] as num,
  );
}

Map<String, dynamic> _$CommentToJson(Comment instance) => <String, dynamic>{
      'score': instance.score,
      'count': instance.count,
      'averageScore': instance.averageScore,
    };
