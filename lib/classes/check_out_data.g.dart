// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_out_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Checkout _$CheckoutFromJson(Map<String, dynamic> json) {
  return Checkout(
    store: json['store'] == null
        ? null
        : StoreDetail.fromJson(json['store'] as Map<String, dynamic>),
    user: json['user'] == null
        ? null
        : HoWalletAmount.fromJson(json['user'] as Map<String, dynamic>),
    credit: json['credit'] == null
        ? null
        : CreditCard.fromJson(json['credit'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CheckoutToJson(Checkout instance) => <String, dynamic>{
      'store': instance.store,
      'user': instance.user,
      'credit': instance.credit,
    };
