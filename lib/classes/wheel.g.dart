// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wheel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Wheel _$WheelFromJson(Map<String, dynamic> json) {
  return Wheel(
    name: json['name'] as String,
    type: json['type'] as String,
    img: json['img'] == null
        ? null
        : ImageInfo.fromJson(json['img'] as Map<String, dynamic>),
    val: json['val'] as String,
  )..content = json['content'] as String;
}

Map<String, dynamic> _$WheelToJson(Wheel instance) => <String, dynamic>{
      'name': instance.name,
      'type': instance.type,
      'img': instance.img,
      'val': instance.val,
      'content': instance.content,
    };
