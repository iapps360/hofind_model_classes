// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'announce_personal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnnouncePersonal _$AnnouncePersonalFromJson(Map<String, dynamic> json) {
  return AnnouncePersonal(
    type: json['type'] as int,
    read: json['read'] as bool,
    userUid: json['userUid'] as String,
    title: json['title'] as String,
    content: json['content'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uid: json['uid'] as String,
  );
}

Map<String, dynamic> _$AnnouncePersonalToJson(AnnouncePersonal instance) =>
    <String, dynamic>{
      'type': instance.type,
      'read': instance.read,
      'userUid': instance.userUid,
      'title': instance.title,
      'content': instance.content,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uid': instance.uid,
    };
