// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'point_gift.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointGift _$PointGiftFromJson(Map<String, dynamic> json) {
  return PointGift(
    useDate: json['useDate'] == null
        ? null
        : _UseDate.fromJson(json['useDate'] as Map<String, dynamic>),
    type: json['type'] as String,
    qty: json['qty'] as int,
    img: json['img'] == null
        ? null
        : ImageInfo.fromJson(json['img'] as Map<String, dynamic>),
    name: json['name'] as String,
    point: json['point'] as int,
    content: json['content'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    uid: json['uid'] as String,
  );
}

Map<String, dynamic> _$PointGiftToJson(PointGift instance) => <String, dynamic>{
      'useDate': instance.useDate,
      'type': instance.type,
      'qty': instance.qty,
      'img': instance.img,
      'name': instance.name,
      'point': instance.point,
      'content': instance.content,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'uid': instance.uid,
    };

_UseDate _$_UseDateFromJson(Map<String, dynamic> json) {
  return _UseDate(
    start:
        json['start'] == null ? null : DateTime.parse(json['start'] as String),
    end: json['end'] == null ? null : DateTime.parse(json['end'] as String),
  );
}

Map<String, dynamic> _$_UseDateToJson(_UseDate instance) => <String, dynamic>{
      'start': instance.start?.toIso8601String(),
      'end': instance.end?.toIso8601String(),
    };
