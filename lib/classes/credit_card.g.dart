// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credit_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreditCard _$CreditCardFromJson(Map<String, dynamic> json) {
  return CreditCard(
    no: json['no'] as String,
    expired: json['expired'] == null
        ? null
        : DateTime.parse(json['expired'] as String),
  );
}

Map<String, dynamic> _$CreditCardToJson(CreditCard instance) =>
    <String, dynamic>{
      'no': instance.no,
      'expired': instance.expired?.toIso8601String(),
    };
